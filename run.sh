#!/bin/bash

cd elmer && ElmerSolver && cd ..
if [ $? -ne 0 ]
then
    exit 1
fi
    
python/venv/bin/python python/simulate_and_compare.py
if [ $? -ne 0 ]
then
    exit 1
fi
    
exit 0

